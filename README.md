# Devhelp integration for Sublime Text

[Devhelp][] integration for Sublime Text. Initially based on [Zeal plugin][].

[Devhelp][] is a GNOME app for browsing and searching API documentation.

[Devhelp]: https://wiki.gnome.org/Apps/Devhelp
[Zeal plugin]: https://github.com/SublimeText/Zeal

## Usage

- <kbd>F1</kbd> - Search for the currently selected word.

- <kbd>Shift</kbd>&nbsp;<kbd>F1</kbd> - Custom search

## Installation

Not on Package Control yet.

### Using Git

Go to your Sublime Text `Packages` directory and clone the repository using the command below:

    $ git clone https://github.com/amezin/sublime-devhelp.git Devhelp

## Configuration

Select `Preferences: Devhelp Settings` form the command palette
to open the configuration files.

If your devhelp executable cannot be found by default,
change the `devhelp_command` setting.
